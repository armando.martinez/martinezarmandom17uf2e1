using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class CanonRotation : MonoBehaviour
{
    public Vector3 _maxRotation;
    public Vector3 _minRotation;
    private float offset = -51.6f;
    public GameObject ShootPoint;
    public GameObject Bullet;
    public float ProjectileSpeed = 0;
    public float MaxSpeed;
    public float MinSpeed;
    public GameObject PotencyBar;
    private float initialScaleX;

    public Camera cam;


    private void Awake()
    {
        initialScaleX = PotencyBar.transform.localScale.x;
    }
    void Update()
    {
        var mousePos = (Input.mousePosition);
        var dist = mousePos - Bullet.transform.position;
        var ang = (Mathf.Atan2(dist.y, dist.x) * 180f / Mathf.PI + offset);
        transform.rotation = ang < _maxRotation.z && ang > _minRotation.z ? Quaternion.Euler(0,0,ang) : Quaternion.identity;
        if (Input.GetMouseButton(0))
        {
            ProjectileSpeed = ProjectileSpeed < MaxSpeed ? ProjectileSpeed + 0.04f : ProjectileSpeed = MaxSpeed;
        }
        if (Input.GetMouseButtonUp(0))
        {
            var projectile = Instantiate(Bullet, ShootPoint.transform.position, ShootPoint.transform.rotation);
            projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(ProjectileSpeed * dist.normalized.x, ProjectileSpeed * dist.normalized.y); //quina velocitat ha de tenir la bala? s'ha de fer alguna cosa al vector direcci�?
            ProjectileSpeed = MinSpeed;
        }
        CalculateBarScale();

    }
    public void CalculateBarScale()
    {
        PotencyBar.transform.localScale = new Vector3(Mathf.Lerp(0, initialScaleX, ProjectileSpeed / MaxSpeed),
            transform.localScale.y,
            transform.localScale.z);
    }
}
